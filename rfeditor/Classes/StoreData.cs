﻿using System;
using System.IO;

namespace RFEditor.Classes
{
    public class StoreItemData
    {
        public byte byItemType;
        public byte[] junk = new byte[3];
        public uint dwDTCode;
    }

    public class StoreLimitedItemData
    {
        public byte byItemType;
        public byte[] junk = new byte[3];
        public uint dwDTCode;
        public uint amount;
    }

    public class StoreEntry
    {
        public uint dwIndex;
        public uint dwDTCode;
        public byte byRace;
        public byte[] pName = new byte[32];
        public byte[] pName2 = new byte[32];
        public byte[] pDummyName = new byte[32];
        public byte byMerchantType;
        public byte[] junk1 = new byte[2];
        public float fYAngle;
        public uint AngleToggle;
        public float scale;
        public uint dwMaxItemNum;
        public StoreItemData[] pItemList = new StoreItemData[200];
        public byte[] pButtonFunction = new byte[10];
        public byte[] pDescription = new byte[6];
        public uint multiplier;
        public uint slots;
        public uint rentableTotal;
        public StoreLimitedItemData[] pTempItemList = new StoreLimitedItemData[16];

        public StoreEntry()
        {
            for (int i = 0; i < pItemList.Length; i++)
            {
                pItemList[i] = new StoreItemData();
            }
            for (int i = 0; i < pTempItemList.Length; i++)
            {
                pTempItemList[i] = new StoreLimitedItemData();
            }
        }
    }

    public class StoreData
    {
        public ClientDataHeader fileHeader;
        public StoreEntry[] storeEntry;

        public StoreData(ClientDataHeader header)
        {
            fileHeader = header;
            storeEntry = new StoreEntry[fileHeader.nBlocks];
            for (int i = 0; i < storeEntry.Length; i++)
            {
                storeEntry[i] = new StoreEntry();
            }
        }
        
        public StoreEntry getEntryByIndex(int dwIndex)
        {
            return Array.Find(storeEntry, entry => entry.dwIndex == dwIndex);
        }
    }
}
