﻿using System;
using System.Collections.Generic;

namespace RFEditor.Common
{
    class Definitions
    {
        public static Dictionary<byte, string> itemsTypes = new Dictionary<byte, string>
        {
            {0,"None"},
            {1,"Upper"},
            {2,"Lower"},
            {3,"Gloves"},
            {4,"Boots"},
            {5,"Helmet"},
            {6,"Weapon"},
            {7,"Shield"},
            {8,"Cloaks"},
            {9,"Ring"},
            {10,"Amulets"},
            {11,"Ammo"},
            {12,"MakeTool"},
            {13,"Potion"},
            {14,"Bag"},
            {15,"Battery"},
            {16,"Ore"},
            {17,"Resource"},
            {18,"Force"},
            {19,"UnitKey"},
            {20,"Booty"},
            {21,"Map"},
            {22,"Scrolls"},
            {23,"BattleDungeon"},
            {24,"Animus"},
            {25,"Towers"},
            {26,"Traps"},
            {27,"SeigeKits"},
            {28,"Ticket"},
            {29,"Event"},
            {30,"Recovery"},
            {31,"Box"},
            {32,"FireCracker"},
            {33,"UnMannedMiner"},
            {34,"Radar"},
            {35,"Pagers"},
            {36,"Coupons"},
            {37,"UnitHead"},
            {38,"UnitUpper"},
            {39,"UnitLower"},
            {40,"UnitArms"},
            {41,"UnitShoulder"},
            {42,"UnitBack"},
            {43,"UnitBullet"},
            {255,"255"}
        };

        public static Dictionary<int, string> storeTypes = new Dictionary<int, string>
        {
            {0,"Weapon"},
            {1,"Potion"},
            {2,"Armor"},
            {3,"Decorations?"},
            {4,"Mau"},
            {5,"Ore"},
            {6,"Booty"},
            {7,"Used goods?"},
            {8,""},
            {9,""},
            {10,"Exchange"},
            {11,""},
            {12,"Guild manager"},
            {13,"Guild storage"},
            {14,""},
            {15,""},
            {16,"Force"},
            {17,""},
            {18,"Ether ticket"},
            {19,"Captured Keeper"},
            {20,"Gatekeeper"},
            {21,"UMT"},
            {22,"Portal guide"},
            {23,"General"},
            {24,"Gem collector"},
            {25,"Misc"},
            {26,"Hero"}
        };
    }
}
