﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace RFEditor.Models
{
    
    public class SelectableStoreViewModel : INotifyPropertyChanged
    {
        private bool _isSelected;
        private string dwIndex;
        private string dwDTCode;

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected == value) return;
                _isSelected = value;
                OnPropertyChanged();
            }
        }

        public string Index
        {
            get { return dwIndex; }
            set
            {
                if (dwIndex == value) return;
                dwIndex = value;
                OnPropertyChanged();
            }
        }

        public string Code
        {
            get { return dwDTCode; }
            set
            {
                if (dwDTCode == value) return;
                dwDTCode = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
