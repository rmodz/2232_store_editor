﻿using MaterialDesignThemes.Wpf;
using System;
using RFEditor.Views;

namespace RFEditor.Models
{
    public class MainWindowViewModel
    {
        public MainWindowViewModel(ISnackbarMessageQueue snackbarMessageQueue)
        {
            if (snackbarMessageQueue == null) throw new ArgumentNullException(nameof(snackbarMessageQueue));

            MenuItems = new[]
            {
                //new MenuItem("Home", new Home()),
                new MenuItem("Store editor", new StoreEditor() { DataContext = new StoreTreeViewModel(snackbarMessageQueue) })
            };
        }

        public MenuItem[] MenuItems { get; }
    }
}