﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using Microsoft.Win32;
using GongSolutions.Wpf.DragDrop;
using RFEditor.Utilities;
using RFEditor.Classes;
using System.IO;
using System;
using System.Text;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using MaterialDesignThemes.Wpf;
using RFEditor.Common;

namespace RFEditor.Models
{
    public class NpcStore : INotifyPropertyChanged, IDropTarget
    {
        private ObservableCollection<StoreGridViewModel> _storeGridViewItems;
        private bool hasChanges = false;
        private bool hasErrors = false;
        private int itemCount = 0;

        public NpcStore(StoreEntry clientData, StoreListEntry serverData, NDStoreEntry ndClientData)
        {

            StoreGridViewItems = new ObservableCollection<StoreGridViewModel> { };
            StoreGridViewItems.CollectionChanged += GridView_CollectionChanged;

            Name = System.Text.Encoding.Default.GetString(ndClientData.NPC).TrimEnd('\0');
            Code = System.Text.Encoding.Default.GetString(ndClientData.Name).TrimEnd('\0');
            Index = serverData.m_dwIndex;

            int i = 0;
            int itemCount = 0;
            bool stopCount = false;
            foreach (StoreItemData data in clientData.pItemList)
            {
                StoreListItemData item = serverData.m_strItemlist[i];
                string serverId = System.Text.Encoding.Default.GetString(item.itemId).TrimEnd('\0');

                if (!stopCount && serverId != "0")
                {
                    itemCount++;
                }
                else
                {
                    stopCount = true;
                }
                StoreGridViewModel storeGridViewModel =  new StoreGridViewModel(data.byItemType, data.dwDTCode, serverId, i);
                StoreGridViewItems.Add(storeGridViewModel);

                if (!HasErrors && storeGridViewModel.Error)
                {
                    HasErrors = true;
                }
                i++;
            }
            
            ItemCount = itemCount;
        }
       
        public string Name { get; set; }
        public string Code { get; set; }
        public int Index { get; set; }

        public int ItemCount
        {
            get { return itemCount; }
            set
            {
                if (itemCount == value) return;
                itemCount = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<StoreGridViewModel> StoreGridViewItems
        {
            get { return _storeGridViewItems; }
            set
            {
                if (_storeGridViewItems == value) return;
                _storeGridViewItems = value;
                OnPropertyChanged();
            }
        }

        public bool HasChanges
        {
            get { return hasChanges; }
            set
            {
                if (hasChanges == value) return;
                hasChanges = value;
                OnPropertyChanged();
            }
        }

        public bool HasErrors
        {
            get { return hasErrors; }
            set
            {
                if (hasErrors == value) return;
                hasErrors = value;
                OnPropertyChanged();
            }
        }

        private void GridView_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (StoreGridViewModel newItem in e.NewItems)
                {
                    newItem.PropertyChanged += Newitem_PropertyChanged;
                }
            }
        }

        private void Newitem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            StoreGridViewModel storeGridViewModel = sender as StoreGridViewModel;
            if (storeGridViewModel.Updated)
            {
                int itemCount = 0;
                bool stopCount = false;
                foreach (StoreGridViewModel gridViewItem in StoreGridViewItems)
                {
                    if (!stopCount && gridViewItem.ItemId != "")
                    {
                        itemCount++;
                    }
                    else
                    {
                        stopCount = true;
                    }
                }
                ItemCount = itemCount;
                HasChanges = true;
            }
            if (storeGridViewModel.Error)
            {
                HasErrors = true;
            }
        }

        void IDropTarget.DragOver(IDropInfo dropInfo)
        {
            GongSolutions.Wpf.DragDrop.DragDrop.DefaultDropHandler.DragOver(dropInfo);
        }

        void IDropTarget.Drop(IDropInfo dropInfo)
        {
            StoreGridViewModel sourceItem = dropInfo.Data as StoreGridViewModel;
            StoreGridViewModel targetItem = dropInfo.TargetItem as StoreGridViewModel;

            int start = sourceItem.Index;
            int end = targetItem.Index;

            if (end < start)
            {
                int _start = start;
                start = end;
                end = _start;
            }

            foreach (StoreGridViewModel storeGridViewModel in StoreGridViewItems)
            {
                if (storeGridViewModel.Index >= start && storeGridViewModel.Index <= end)
                {
                    storeGridViewModel.Updated = true;
                }
            }
            GongSolutions.Wpf.DragDrop.DragDrop.DefaultDropHandler.Drop(dropInfo);
            sourceItem.Updated = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public sealed class Race
    {
        public Race(string name, ObservableCollection<NpcStore> npcStores)
        {
            Name = name;
            NpcStores = npcStores;
        }

        public string Name { get; }

        public ObservableCollection<NpcStore> NpcStores { get; }
    }

    public class StoreTreeViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Race> Races { get; set; }
        private readonly ISnackbarMessageQueue _snackbarMessageQueue;

        public Command LoadStore { get; }
        public Command Save { get; }
        public DataFile dataFile = new DataFile();
       
        public void UpdateStoreData()
        {
            foreach (Race race in Races)
            {
                foreach (NpcStore npcStore in race.NpcStores)
                {
                    StoreItemData[] pItemList = dataFile.storeData.storeEntry[npcStore.Index].pItemList;
                    StoreListItemData[] m_strItemlist = dataFile.storeListData.storeListEntry[npcStore.Index].m_strItemlist;
                    int index = 0;
                    foreach (StoreGridViewModel storeGridViewItem in npcStore.StoreGridViewItems)
                    {
                        string clientId = storeGridViewItem.ClientId == "" ? "0" : storeGridViewItem.ClientId;
                        string itemId = storeGridViewItem.ItemId == "" ? "0" : storeGridViewItem.ItemId;

                        pItemList[index].byItemType = storeGridViewItem.Type;
                        pItemList[index].dwDTCode = Convert.ToUInt32(clientId, 16);
                        m_strItemlist[index].itemId = Encoding.ASCII.GetBytes(itemId.PadRight(64, '\0'));
                        index++;
                    }
                }
            }
        }

        static Dictionary<byte, string> items;
        public static Dictionary<byte, string> ItemTypes
        {
            get
            {
                return items ?? (items = Definitions.itemsTypes);
            }
        }

        public StoreTreeViewModel(ISnackbarMessageQueue snackbarMessageQueue)
        {
            _snackbarMessageQueue = snackbarMessageQueue ?? throw new ArgumentNullException(nameof(snackbarMessageQueue));
            Races = new ObservableCollection<Race>{};

            Save = new Command(_ =>
            {
                UpdateStoreData();

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "RF *.dat|StoreList_new.DAT";
                saveFileDialog.FileName = "StoreList_new";
                if (saveFileDialog.ShowDialog() == true)
                {
                    BinaryWriter storeListWriter = new BinaryWriter(File.OpenWrite(saveFileDialog.FileName));
                    BinaryUtils.Serialize(storeListWriter, dataFile.storeListData);
                    storeListWriter.Flush();
                    storeListWriter.Close();

                    saveFileDialog.Filter = "RF *.dat|Store_new.DAT";
                    saveFileDialog.FileName = "Store_new";
                    if (saveFileDialog.ShowDialog() == true)
                    {
                        BinaryWriter storeWriter = new BinaryWriter(File.OpenWrite(saveFileDialog.FileName));
                        BinaryUtils.Serialize(storeWriter, dataFile.storeData);
                        storeWriter.Flush();
                        storeWriter.Close();
                    }
                }
            });

            LoadStore = new Command(_ =>
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "RF Store.dat|Store.dat";
                if (openFileDialog.ShowDialog() == true)
                {
                    dataFile.load(openFileDialog.FileName, DataFile.CLIENT_STORE_DATA);
                    if (dataFile.storeData.storeEntry.Length > 0)
                    {
                        openFileDialog.Filter = "RF NDStore.dat|NDStore.dat";
                        if (openFileDialog.ShowDialog() == true)
                        {
                            dataFile.load(openFileDialog.FileName, DataFile.NDCLIENT_STORE_DATA);
                            if (dataFile.ndStoreData.ndStoreEntry.Length > 0)
                            {
                                openFileDialog.Filter = "RF StoreList.dat|StoreList.dat";
                                if (openFileDialog.ShowDialog() == true)
                                {
                                    dataFile.load(openFileDialog.FileName, DataFile.SERVER_STORE_DATA);

                                    Dictionary<int, ObservableCollection<NpcStore>> npcTypes = new Dictionary<int, ObservableCollection<NpcStore>> { };

                                    foreach (StoreListEntry serverData in dataFile.storeListData.storeListEntry)
                                    {
                                        StoreEntry clientData = dataFile.storeData.getEntryByIndex(serverData.m_dwIndex);
                                        NDStoreEntry ndClientData = dataFile.ndStoreData.getEntryByIndex(serverData.m_dwIndex);

                                        NpcStore npcStore = new NpcStore(clientData, serverData, ndClientData);
                                        if (!npcTypes.ContainsKey(serverData.m_nStore_trade))
                                        {
                                            npcTypes.Add(serverData.m_nStore_trade, new ObservableCollection<NpcStore>() { npcStore });
                                        } else
                                        {
                                            npcTypes[serverData.m_nStore_trade].Add(npcStore);
                                        }
                                    }
                                    foreach (KeyValuePair<int, ObservableCollection<NpcStore>> entry in npcTypes)
                                    {
                                        Races.Add(new Race(Definitions.storeTypes[entry.Key], entry.Value));
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
