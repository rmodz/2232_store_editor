﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RFEditor.Utilities
{
    public class BinaryUtils
    {
        public static void Serialize(BinaryWriter writer, object obj)
        {
            foreach (var value in from field in obj.GetType().GetFields() select field.GetValue(obj))
            {
                if (value is int)
                {
                    writer.Write((int)value);
                }
                else if (value is uint)
                {
                    writer.Write((uint)value);
                }
                else if (value is byte[])
                {
                    writer.Write((byte[])value);
                }
                else if (value is byte)
                {
                    writer.Write((byte)value);
                }
                else if (value is float)
                {
                    writer.Write((float)value);
                }
                else if (value is Array)
                {
                    foreach (var child in (Array)value)
                    {
                        Serialize(writer, child);
                    }
                }

                else if (!value.GetType().IsPrimitive && value is Object)
                {
                    Serialize(writer, value);
                }
            }
        }

        public static void UnSerialize(BinaryReader reader, object obj)
        {
            if (obj is Array)
            {
                foreach (var child in (Array)obj)
                {
                    UnSerializeChildren(reader, child);
                }
            } else
            {
                UnSerializeChildren(reader, obj);
            }
        }

        private static byte[] ReadAscii(BinaryReader input)
        {
            List<byte> strBytes = new List<byte>();
            int b;
            while ((b = input.ReadByte()) != 0x00)
                strBytes.Add((byte)b);
            return strBytes.ToArray();
        }

        private static void UnSerializeChildren(BinaryReader reader, object obj)
        {
            Type type = obj.GetType();
            FieldInfo[] fields = type.GetFields();

            foreach (FieldInfo field in fields)
            {
                var value = field.GetValue(obj);
                if (value is int)
                {
                    field.SetValue(obj, reader.ReadInt32());
                }
                else if (value is uint)
                {
                    field.SetValue(obj, reader.ReadUInt32());
                }
                else if (value is byte)
                {
                    field.SetValue(obj, reader.ReadByte());
                }
                else if (value is byte[])
                {
                    Array val = value as Array;
                    if (val.Length > 0)
                    {
                        field.SetValue(obj, reader.ReadBytes(val.Length));
                    }
                    else
                    {
                        var x = ReadAscii(reader);
                        field.SetValue(obj, x);
                    }
                }
                else if (value is float)
                {
                    field.SetValue(obj, reader.ReadSingle());
                }
                else if (value is Array)
                {
                    foreach (var child in (Array)value)
                    {
                        UnSerializeChildren(reader, child);
                    }
                }
            }
        }
    }
}
